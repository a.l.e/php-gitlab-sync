<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

header("Content-Type: text/plain");

$config = include('config.php');

$data = file_get_contents('php://input');
// file_put_contents('server.json', json_encode($_SERVER));
// file_put_contents('log.json', $data);

if (!array_key_exists('HTTP_X_GITLAB_EVENT', $_SERVER) || $_SERVER['HTTP_X_GITLAB_EVENT'] !== 'Push Hook') {
    http_response_code(403);
    die('invalid action');
}
if (!array_key_exists('HTTP_X_GITLAB_TOKEN', $_SERVER) || $_SERVER['HTTP_X_GITLAB_TOKEN'] !== $config['secret']) {
    http_response_code(403);
    die('invalid secret');
}

$data = json_decode($data, true);

$repository = $data['project']['path_with_namespace'];

if (!array_key_exists($repository, $config['target'])) {
    http_response_code(403);
    die('unsupported repository: '. $repository);
}

$branch = explode('/', $data['ref'])[2];

if (!array_key_exists($branch, $config['target'][$repository])) {
    http_response_code(403);
    die('unsupported branch: '. $branch);
}

if (is_null($config['target'][$repository])) {
    die('ignored branch');
}

$path = rtrim($config['target'][$repository][$branch], '/');

if ($path === '' || !is_dir($path)) {
    http_response_code(403);
    die('invalid target path: '. $path);
}

if (!is_dir($path.'/.git')) {
    http_response_code(403);
    die('the target path is not a git repository: '. $path);
}

shell_exec( 'cd '.$path.' && git reset --hard HEAD && git pull' );
shell_exec( 'cd '.$path.' && git fetch --depth=1 && git reflog expire --expire-unreachable=now --all && git gc --aggressive --prune=all' );

echo('ok');
