# php-gitlab-sync

Simple PHP script (and instructions) for keeping a PHP project in sync with a Gitlab repository without using Git.

- It only manages files that are in the Git repository.
- Local changes on tracked files will be overwritten as soon as the file gets modified in the repository

You can use one sync script for multiple projects and branches, each in their own directory.

## Installing

- Add this script to your website (as an example as `sync/index.php`).
- Create a `config.php` based on `config-demo.php`.

## Setup the webhook on Gitlab

- In _Settings > Webhooks_, add a webhook targeting this PHP script.
  - _Url_: path to this script (`http://your-server.org/sync`)
  - _Secret Token_: define a secret for authentication; you will put the same value in the `config.php` file on the server.
  - Check the checkbox to trigger _Push events_ (set the branch if you have multiple ones).

Now, each time your repository gets pushed (or edited in the Web editor), the server will pull the files that have been modified (or remove the deleted ones).

## Todo

- Delete empty directories.
- Add an option to use git or not to update the project.

## Notes

If we ever need curl to get the content:

```php
function get_url_content($url) {
    $result = '';
    // echo('<pre>url: '.print_r($url, 1).'</pre>');
    // $this->log[] = '>> '.$url;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, 'aoloe/deploy-git');
    // curl_setopt($ch, CURLOPT_HEADER, true);
    // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    $result = curl_exec($ch);
    $info = curl_getinfo($ch);
    // \Aoloe\debug('info', $info);
    if (($info['http_code'] == '404') || ($info['http_code'] == '403')) {
        $result = false;
    }
    curl_close($ch);
    // echo('<pre>result: '.print_r($result, 1).'</pre>');
    return $result;
}
```

If we ever need the API:

```php
$project_id = $data['project_id'];
$source_url = "https://gitlab.com/api/v4/projects/$project_id/repository/files/$filename?ref=$branch";
$content = json_decode($content, true);
file_put_contents($target_filename, base64_decode($content['content']));
```

using the API pulls a json with some info and a base64 encoded content
