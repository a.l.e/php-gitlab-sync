<?php
return [
    'target' => [
        'username/repository_name' => [
            'master' => '/path/to/the/deployment',
            'basedir' => 'htdocs/',
        ]
    ],
    'secret' => 'a real secret',
    'script' => [
        'username/repository_name' => [
            'after' => [
                'php7.3 ../forums/bin/phpbbcli.php cache:purge  &> /dev/null &',
            ]
        ]
    ]
];
