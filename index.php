<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// https://github.com/symfony/polyfill/blob/main/src/Php80/Php80.php
function str_starts_with(string $haystack, string $needle): bool {
    return 0 === strncmp($haystack, $needle, \strlen($needle));
}

header("Content-Type: text/plain");

$config = include('config.php');

$data = file_get_contents('php://input');
// file_put_contents('server.json', json_encode($_SERVER));
// file_put_contents('log.json', $data);

if (!array_key_exists('HTTP_X_GITLAB_EVENT', $_SERVER) || $_SERVER['HTTP_X_GITLAB_EVENT'] !== 'Push Hook') {
    http_response_code(403);
    die('invalid action');
}
if (!array_key_exists('HTTP_X_GITLAB_TOKEN', $_SERVER) || $_SERVER['HTTP_X_GITLAB_TOKEN'] !== $config['secret']) {
    http_response_code(403);
    die('invalid secret');
}

$data = json_decode($data, true);

$repository = $data['project']['path_with_namespace'];

if (!array_key_exists($repository, $config['target'])) {
    http_response_code(403);
    die('unsupported repository: '. $repository);
}

$branch = explode('/', $data['ref'])[2];

if (!array_key_exists($branch, $config['target'][$repository])) {
    http_response_code(403);
    die('unsupported branch: '. $branch);
}

if (is_null($config['target'][$repository])) {
    die('ignored branch');
}

$path = rtrim($config['target'][$repository][$branch], '/');

if ($path === '' || !is_dir($path)) {
    http_response_code(403);
    die('invalid target path: '. $path);
}

$queue = [];
foreach ($data['commits'] as $commit) {
    foreach (array_merge($commit['added'], $commit['modified']) as $item) {
        $queue[$item] = 'get';
    }
    foreach ($commit['removed'] as $item) {
        $queue[$item] = 'remove';
    }
}

// file_put_contents('queue.json', json_encode($queue));

function is_valid_filename($path) {
    $parts = explode('/', str_replace('\\', '/', $path));
    foreach ($parts as $part) {
        if ($part === '') {
            return false;
        } elseif ($part === '..') {
            return false;
        }
    }
    return true;
}

/**
 * returns 200 if the file exists, 404 if it does not
 */
function get_http_response_code($url) {
    $headers = get_headers($url);
    if ($headers === false) {
        return 0;
    }
    // echo('code '.substr($headers[0], 9, 3)."\n");
    return intval(substr($headers[0], 9, 3));
}

/**
 * if basedir is not null,
 * - if path starts with it, return the string with the basedir removed
 * - otherwise return null
 * otherwise return the string
 */
function get_without_basedir($basedir, $path) {
    if (is_null($basedir)) {
        return $path;
    }
    if (!str_starts_with($path, $basedir)) {
        return null;
    }

    return substr($path, strlen($basedir));
}

$actions = [];

$target_path = rtrim($config['target'][$repository][$branch], '/');

foreach ($queue as $filename => $action) {
    if (!is_valid_filename($filename)) {
        continue;
    }
    $source_url = "https://gitlab.com/$repository/-/raw/$branch/$filename?inline=false";

    // ignore files that are outside of basedir
    $basedir_filename = get_without_basedir($config['target'][$repository]['basedir'] ?? null, $filename);
    // file_put_contents('log.txt', "filename: $filename\nbasedir_filename: $basedir_filename\n", FILE_APPEND);
    if (is_null($basedir_filename)) {
        continue;
    }

    $target_filename = $target_path.'/'.$basedir_filename;
    if ($action === 'remove') {
        if (get_http_response_code($source_url) !== 404) {
            echo("$filename not deleted\n");
            continue;
        }
        if (file_exists($target_filename)) {
            unlink($target_filename);
        }
    } elseif ($action === 'get') {
        if (get_http_response_code($source_url) !== 200) {
            echo("$filename from $source_url not updated\n");
            continue;
        }
        $content = file_get_contents($source_url);
        $parent_directory = dirname($target_filename);
        if (!is_dir($parent_directory) && !is_file($parent_directory)) {
            $umask = umask();
            mkdir($parent_directory, 0777, true);
        }
        file_put_contents($target_filename, $content);
        $actions[] = "get $target_filename from $source_url";
    }
}

// file_put_contents('actions.json', json_encode($actions));

if (array_key_exists('script', $config)
    && array_key_exists($repository, $config['script'])
    && array_key_exists('after', $config['script'][$repository])
    ) {
    $script_after = $config['script'][$repository]['after'];
    if (!is_array($script_after)) {
        echo('illformed list of scripts after\n');
    } else {
        foreach ($script_after as $script) {
            shell_exec($script);
        }
    }
}

echo('ok');
